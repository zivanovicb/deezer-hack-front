import React, { Component } from "react";
import { Input, List, Avatar, Button, Icon } from "antd";
import io from "socket.io-client";
import _ from "lodash";
import axios from "axios";
import "./App.scss";
import types from "./types";
import Spinner from "./Spinner";

const REACT_APP_URL = process.env.REACT_APP_URL;
const EXPRESS_SERVER_URL = process.env.REACT_APP_URL;
const API_BASE_URL = `http://localhost:3080/api`;

const APP_ID = process.env.REACT_APP_DEEZER_APP_ID || 270762;

class App extends Component {
  state = {
    loading: true,
    searchResults: [],
    err: null,
    searchValue: "",
    currentTrack: null, // obj that has id,name,length,thumbnail,artist,thumbnail
    currentTrackTimestamp: null,
    playerLoaded: false,

    scene: types.QUEUE_LIST
  };

  nsp = null;

  componentDidMount() {
    const { DZ } = window;
    document.addEventListener("DOMContentLoaded", () => {
      DZ.init({
        appId: APP_ID,
        channelUrl: `${REACT_APP_URL}/channel.html`,
        player: {
          container: "player",
          playlist: true,
          width: 650,
          height: 300,
          onload: function() {
            console.log("loaded");
          }
        }
      });
    });

    this.nsp = io(`localhost:3080`);

    this.nsp.on("connect", () => {
      this.nsp.emit(types.ROOM_JOIN);
    });

    this.nsp.on(types.ROOM_JOIN_SUCCESSFULL, data => {
      console.log("chekckni ", types.CURRENT_TRACK_TIMESTAMP, types.CURRENT_TRACK_DATA);
      const currentTrack = data[types.CURRENT_TRACK_DATA];
      const currentTrackTimestamp = data[types.CURRENT_TRACK_TIMESTAMP];
      const queue = data["queue"];
      this.setState({ currentTrack, currentTrackTimestamp, queue, loading: false });
    });

    this.nsp.on(types.QUEUE_LIST_UPDATED, queue => {
      this.setState({ queue });
    });

    this.nsp.on(types.PLAY_TRACK, data => {
      console.log("DATA ON PLAY TRACK ", data);
      console.log("LEGIT??? ", types.CURRENT_TRACK_DATA);
      this.setState({ currentTrack: data[types.CURRENT_TRACK_DATA] }, () => {
        console.log("SET ON PLAY TRACK", this.state);
      });
    });
    this.getCharts();
  }
  componentDidUpdate(prevProps, prevState) {
    const { DZ } = window;
    if (this.state.searchValue !== prevState.searchValue && this.state.searchValue !== "") {
      this.searchTracks(this.state.searchValue);
    }

    if (!_.isEqual(this.state.currentTrack, prevState.currentTrack) && !!this.state.currentTrack) {
      console.log("forcing click in didupdate");
      DZ.player.playTracks([this.state.currentTrack.id, this.state.currentTrack.id]);
    }
  }

  handleClick = () => {
    const { DZ } = window;
    console.log("about to play ", DZ.player.playAlbum);
    DZ.player.playTracks([3135556, 1152226], function(response) {
      console.log("List of track objects", response.tracks);
    });

    DZ.player.pause();
    DZ.player.play();
    DZ.player.setMute(false);
    DZ.player.setVolume(100);
  };
  componentWillUnmount() {
    if (this.nsp) {
      console.log("Disconnecting");
      this.nsp.disconnect();
    }
  }

  setPlayerLoaded = () => this.setState({ playerLoaded: true }, () => console.log("player loaded ", this.state));

  getCharts = async () => {
    try {
      const r = await axios.get(`${API_BASE_URL}/charts`);
      const charts = this.mapTracks(r.data.tracks);
      this.setState({ charts });
    } catch (err) {
      this.setState({ err });
    }
  };
  searchTracks = async searchValue => {
    console.log("called");
    try {
      const res = await axios.get(`${API_BASE_URL}/search?q=${searchValue}`);
      console.log(res.data);
      const searchResults = this.mapTracks(res.data);
      this.setState({ loading: false, searchResults });
    } catch (err) {
      this.setState({ loading: false, searchResults: null, err: "Something went wrong" });
    }
  };

  mapTracks = tracks =>
    tracks.map((track, i) => ({
      id: track.id,
      name: track.title,
      length: track.duration,
      artist: track.artist.name,
      thumbnail: track.album.cover_big
    }));

  handleChange = e => this.setState({ searchValue: e.target.value });

  handleAddToQueue = track => {
    console.log("wadada ", track);
    // { track_id, track_thumbnail, track_artist, track_name, track_length } ??/
    console.log("ALL TEHSE ", "{ track_id, track_thumbnail, track_artist, track_name, track_length }");
    this.nsp.emit(types.ADD_TO_QUEUE, track);
  };
  renderSearchResults = tracks => {
    const { loading } = this.state;

    return (
      <React.Fragment>
        <h3>Search results</h3>
        <List
          loading={loading}
          itemLayout="horizontal"
          dataSource={tracks}
          renderItem={track => (
            <List.Item
              actions={[<Button type="primary" icon="plus" onClick={this.handleAddToQueue.bind(this, track)} />]}
            >
              <List.Item.Meta
                avatar={<Avatar src={track.thumbnail} />}
                title={<a href="https://ant.design">{track.name}</a>}
                description={track.artist}
              />
            </List.Item>
          )}
        />
      </React.Fragment>
    );
  };

  handlePlayTrack = track => this.nsp.emit(types.PLAY_TRACK, track);
  handleRemoveFromQueue = track => {
    console.log("Removign from queue ", track);
    this.nsp.emit(types.REMOVE_TRACK_FROM_QUEUE, track.id);
  };
  renderQueueList = queue => {
    console.log("KJU ", queue);
    console.log("CURRENT_TRA ", this.state.currentTrack);
    return (
      <div className="queue">
        {queue.length === 0 ? (
          <h3>Your playlist is empty! Start typing to find a track! 🤘</h3>
        ) : (
          <h3> Your playlist! 🎸</h3>
        )}

        {queue.length > 0 && (
          <List
            itemLayout="horizontal"
            dataSource={queue}
            renderItem={track => (
              <List.Item
                actions={[
                  <Button type="primary" icon="caret-right" onClick={this.handlePlayTrack.bind(this, track)} />,
                  <Button type="danger" icon="delete" onClick={this.handleRemoveFromQueue.bind(this, track)} />
                ]}
              >
                <List.Item.Meta
                  avatar={<Avatar src={track.thumbnail} />}
                  title={
                    <a
                      href="https://ant.design"
                      style={{
                        color:
                          !!this.state.currentTrack && this.state.currentTrack.id === track.id ? "#1890ff" : "black"
                      }}
                    >
                      {track.name}
                    </a>
                  }
                  description={track.artist}
                />
              </List.Item>
            )}
          />
        )}
      </div>
    );
  };

  handleChangeScene = scene => {
    this.setState({ scene }, () => {
      console.log("changed scene; ", this.state.scene);
    });
  };

  handleSearchFocus = () => this.setState({ scene: types.SEARCH_RESULTS });

  render() {
    const { loading, searchResults, err, scene } = this.state;
    const queueListButtonProps = {};
    const searchResultsButtonProps = {};

    if (scene === types.QUEUE_LIST) queueListButtonProps.type = "primary";
    if (scene === types.SEARCH_RESULTS) searchResultsButtonProps.type = "primary";
    return (
      <div className="container">
        {loading && <Spinner />}
        {!loading && (
          <React.Fragment>
            <div className="top">
              <Input
                onChange={this.handleChange}
                className="m-b-10"
                onFocus={this.handleSearchFocus}
                placeholder="Start typing track, album..."
              />
              <Button
                className="m-l-5"
                {...queueListButtonProps}
                onClick={this.handleChangeScene.bind(this, types.QUEUE_LIST)}
              >
                <Icon type="bars" />
              </Button>
              <Button
                className="m-l-5"
                {...searchResultsButtonProps}
                onClick={this.handleChangeScene.bind(this, types.SEARCH_RESULTS)}
              >
                <Icon type="search" />
              </Button>
            </div>

            {scene === types.SEARCH_RESULTS && this.renderSearchResults(searchResults)}
            {scene === types.QUEUE_LIST && this.renderQueueList(this.state.queue)}
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default App;
