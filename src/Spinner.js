import React from "react";
import { Spin } from "antd";

const Spinner = () => {
  return (
    <div style={{ width: "100%", minHeight: "100vh", display: "flex", alignItems: "center", justifyContent: "center" }}>
      <Spin />
    </div>
  );
};

export default Spinner;
